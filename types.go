package main

type Stock struct {
	MetaData   MetaData             `json:"Meta Data"`
	TimeSeries map[string]StockInfo `json:"Weekly Time Series"`
}

type MetaData struct {
	Symbol string `json:"2. Symbol"`
}

type StockInfo struct {
	Close string `json:"4. close"`
}
