# Stock Ticker

## Overview

```
$ curl http://stock-ticker.test/stockprice

Symbol: MSFT

Week       | Closing Price
==========================
2023-02-17 |   258.0600
2023-02-10 |   263.1000
2023-02-03 |   258.3500
2023-01-27 |   248.1600
2023-01-20 |   240.2200
2023-01-13 |   239.2300
2023-01-06 |   224.9300

```
This is a simple web service that prints the last 'N' weeks of closing stock prices for a particular stock.
The data is retrieved from [Alpha Vantage's API](https://www.alphavantage.co/documentation/).

The web service servers the following endpoints:

- `/healthcheck` - Returns `200` if the server is up and running.
- `/stockprice` - Prints the closing prices for the latest N weeks.

### Alpha Vantage API Key

Before running the web service you'll need to obtain an API key from Alpha Vantage.
You can claim a free key from:

https://www.alphavantage.co/support/#api-key

### Required environment variables

| Environment Variable | Description                                    | Example |
|----------------------|------------------------------------------------|---------|
| SYMBOL               | The stock's symbol.                            | MSFT    |
| NWEEKS               | Display the closing price for the last N weeks.| 7       |
| APIKEY               | Your API key from Alpha Vantage.               |         |

### Command line arguments

| Argument | Description                                     | Default      |
|----------|-------------------------------------------------|--------------|
| address  | The address that the web server will listen on. | 0.0.0.0:8080 |

## Running the docker image locally

The docker image is published to the GitLab Container Registry [here](https://gitlab.com/dananglin/stock-ticker/container_registry/3916158).
You can pull and run the docker image from the registry with the following command:

```bash
docker run --rm \
    -d \
    -e SYMBOL=MSFT \
    -e NWEEKS=3 \
    -e APIKEY=<YOUR_API_KEY> \
    --publish 8080:8080 \
    --name stock-ticker \
    registry.gitlab.com/dananglin/stock-ticker:v0.1.0
```

Alternatively you can build and run the docker image locally.

```bash
git clone https://gitlab.com/dananglin/stock-ticker.git
cd stock-ticker
docker build -t stock-ticker .
docker run --rm \
    -d \
    -e SYMBOL=MSFT \
    -e NWEEKS=3 \
    -e APIKEY=<YOUR_API_KEY> \
    --publish 8080:8080 \
    --name stock-ticker \
    stock-ticker
```

Now you can go to http://localhost:8080/stockprice in your browser to view a table of the latest weekly closing stock prices for `MSFT`.

## Deploying the web service on Minikube

The web service can be deployed on any distribution of Kubernetes but the below guide will focus on deployment onto a Minikube cluster.
The Kubernetes manifest can viewed [here](https://gitlab.com/dananglin/stock-ticker/-/blob/main/kubernetes/manifest.yaml) and you can download and modify it as you wish.

1. Follow [these instructions](https://k8s-docs.netlify.app/en/docs/tasks/tools/install-minikube/) to install and start a Minikube cluster.
2. Set up ingress using either the [Ingress DNS](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/) or [Kong Ingress](https://minikube.sigs.k8s.io/docs/handbook/addons/kong-ingress/) addon.
3. Apply the Kubernetes manifest to your Minikube cluster.
    ```bash
    kubectl apply -f https://gitlab.com/dananglin/stock-ticker/-/raw/main/kubernetes/manifest.yaml
    ```
4. Wait until the pods are running and in a 'Ready' state.
    ```bash
    $ kubectl -n stock-ticker get pods
     NAME                            READY   STATUS    RESTARTS   AGE
     stock-ticker-656859cf74-bb8w6   1/1     Running   0          29s
     stock-ticker-656859cf74-nvnbf   1/1     Running   0          29s
     stock-ticker-656859cf74-p6xzk   1/1     Running   0          29s
    ```
5. Go to http://stock-ticker.<YOUR_TLD>/stockprice from your browser.
