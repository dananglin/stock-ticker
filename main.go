package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	requestURLFormat string = "https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol=%s&apikey=%s"
)

var tableFormat = `
Symbol: %s

Week       | Closing Price
==========================
`

func main() {
	var address string

	flag.StringVar(&address, "address", "0.0.0.0:8080", "the web server's listening address")
	flag.Parse()

	symbol, err := getEnvironment("SYMBOL")
	if err != nil {
		log.Fatalf("ERROR: %v.\n", err)
	}

	apiKey, err := getEnvironment("APIKEY")
	if err != nil {
		log.Fatalf("ERROR: %v.\n", err)
	}

	nWeeksString, err := getEnvironment("NWEEKS")
	if err != nil {
		log.Fatalf("ERROR: %v.\n", err)
	}

	nWeeks, err := strconv.Atoi(nWeeksString)
	if err != nil {
		log.Fatalf("ERROR: Unable to parse the number of days from %s, %v\n", nWeeksString, err)
	}

	http.HandleFunc("/healthcheck", healthCheckHandleFunc)
	http.HandleFunc("/stockprice", stockPriceHandleFunc(apiKey, symbol, nWeeks))

	log.Printf("The web server is listening on %s.\n", address)

	log.Fatal(http.ListenAndServe(address, nil))
}


func getEnvironment(key string) (string, error) {
	value := os.Getenv(key)
	if value == "" {
		return "", fmt.Errorf("the environment variable %s is not set", key)
	}

	return value, nil
}
