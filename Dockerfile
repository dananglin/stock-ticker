FROM golang:1.19.6-alpine AS builder

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

ADD *.go /workspace/
ADD go.mod /workspace/

WORKDIR /workspace

RUN go build -a -v -ldflags="-s -w" -o /workspace/stock-ticker .

FROM gcr.io/distroless/static-debian11

COPY --from=builder /workspace/stock-ticker /stock-ticker

USER 1000

ENTRYPOINT ["/stock-ticker"]

HEALTHCHECK NONE
